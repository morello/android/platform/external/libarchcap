/*
 * Copyright (c) 2020 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

/**
 * @file archcap-asm.h
 * @brief Defines part of the external archcap interface
 *        compatible with assembler.
 */

#if !defined(__ARCHCAP_INTERNAL_HEADERS_ALLOWED) && !defined(__ASSEMBLER__)
#error "Please, include <archcap.h> instead"
#endif /* !__ARCHCAP_INTERNAL_HEADERS_ALLOWED && !__ASSEMBLER__ */

#if defined(__CHERI__)

#define ARCHCAP_CAPABILITIES_AVAILABLE 1

#if defined(__CHERI_PURE_CAPABILITY__)
#define ARCHCAP_ABI_PURECAP 1
#define ARCHCAP_ABI_HYBRID 0
#else /* !__CHERI_PURE_CAPABILITY__ */
#define ARCHCAP_ABI_PURECAP 0
#define ARCHCAP_ABI_HYBRID 1
#endif /* !__CHERI_PURE_CAPABILITY__ */

#else /* !__CHERI__ */

#define ARCHCAP_CAPABILITIES_AVAILABLE 0
#define ARCHCAP_ABI_PURECAP 0
#define ARCHCAP_ABI_HYBRID 0

#endif /* !__CHERI__ */

/* Interface: permission bits */
#define ARCHCAP_PERM_GLOBAL                         __CHERI_CAP_PERMISSION_GLOBAL__
#define ARCHCAP_PERM_EXECUTE                        __CHERI_CAP_PERMISSION_PERMIT_EXECUTE__
#define ARCHCAP_PERM_LOAD                           __CHERI_CAP_PERMISSION_PERMIT_LOAD__
#define ARCHCAP_PERM_STORE                          __CHERI_CAP_PERMISSION_PERMIT_STORE__
#define ARCHCAP_PERM_LOAD_CAP                       __CHERI_CAP_PERMISSION_PERMIT_LOAD_CAPABILITY__
#define ARCHCAP_PERM_STORE_CAP                      __CHERI_CAP_PERMISSION_PERMIT_STORE_CAPABILITY__
#define ARCHCAP_PERM_STORE_LOCAL_CAP                __CHERI_CAP_PERMISSION_PERMIT_STORE_LOCAL__
#define ARCHCAP_PERM_SEAL                           __CHERI_CAP_PERMISSION_PERMIT_SEAL__
#define ARCHCAP_PERM_UNSEAL                         __CHERI_CAP_PERMISSION_PERMIT_UNSEAL__
#define ARCHCAP_PERM_SYSTEM                         __CHERI_CAP_PERMISSION_ACCESS_SYSTEM_REGISTERS__

#define ARCHCAP_PERM_MORELLO_EXECUTIVE              __ARM_CAP_PERMISSION_EXECUTIVE__
#define ARCHCAP_PERM_MORELLO_MUTABLE_LOAD           __ARM_CAP_PERMISSION_MUTABLE_LOAD__
#define ARCHCAP_PERM_MORELLO_COMPARTMENT_ID         __ARM_CAP_PERMISSION_COMPARTMENT_ID__
#define ARCHCAP_PERM_MORELLO_BRANCH_SEALED_PAIR     __ARM_CAP_PERMISSION_BRANCH_SEALED_PAIR__

#define ARCHCAP_PERM_MORELLO_USER_BIT_0             (1 << 2)
#define ARCHCAP_PERM_MORELLO_USER_BIT_1             (1 << 3)
#define ARCHCAP_PERM_MORELLO_USER_BIT_2             (1 << 4)
#define ARCHCAP_PERM_MORELLO_USER_BIT_3             (1 << 5)

#define ARCHCAP_PERM_MASK_ALL                       (ARCHCAP_PERM_GLOBAL                       \
                                                     | ARCHCAP_PERM_SYSTEM                     \
                                                     | ARCHCAP_PERM_UNSEAL                     \
                                                     | ARCHCAP_PERM_SEAL                       \
                                                     | ARCHCAP_PERM_STORE_LOCAL_CAP            \
                                                     | ARCHCAP_PERM_STORE_CAP                  \
                                                     | ARCHCAP_PERM_LOAD_CAP                   \
                                                     | ARCHCAP_PERM_EXECUTE                    \
                                                     | ARCHCAP_PERM_STORE                      \
                                                     | ARCHCAP_PERM_LOAD                       \
                                                     | ARCHCAP_PERM_MORELLO_EXECUTIVE          \
                                                     | ARCHCAP_PERM_MORELLO_MUTABLE_LOAD       \
                                                     | ARCHCAP_PERM_MORELLO_COMPARTMENT_ID     \
                                                     | ARCHCAP_PERM_MORELLO_BRANCH_SEALED_PAIR \
                                                     | ARCHCAP_PERM_MORELLO_USER_BIT_0         \
                                                     | ARCHCAP_PERM_MORELLO_USER_BIT_1         \
                                                     | ARCHCAP_PERM_MORELLO_USER_BIT_2         \
                                                     | ARCHCAP_PERM_MORELLO_USER_BIT_3)

/* Object type constants */
#define ARCHCAP_MORELLO_OTYPE_NUM_BITS              15
#define ARCHCAP_MORELLO_OTYPE_MAX                   ((1 << ARCHCAP_MORELLO_OTYPE_NUM_BITS) - 1)
#define ARCHCAP_SEAL_TYPE_SENTRY                    1 /* same as CHERI_OTYPE_SENTRY */
#define ARCHCAP_SEAL_TYPE_MORELLO_RB                ARCHCAP_SEAL_TYPE_SENTRY
#define ARCHCAP_SEAL_TYPE_MORELLO_LPB               2
#define ARCHCAP_SEAL_TYPE_MORELLO_LB                3
