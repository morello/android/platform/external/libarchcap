/*
 * Copyright (c) 2020 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

/** Disable run-time assertions by default.
 *
 * Enabling run-time assertions imposes dependency on some headers, see below.
 */
#define ARCHCAP_NO_RUNTIME_ASSERTIONS

#if !defined(ARCHCAP_NO_RUNTIME_ASSERTIONS) \
    && defined(__linux__) && defined(__aarch64__)
#include <asm/unistd.h>
#include <unistd.h>

#define __ARCHCAP_PRINT_SUPPORTED 1
#else /* !__linux__ || !__aarch64__ */
#define __ARCHCAP_PRINT_SUPPORTED 0
#endif /* !__linux__ || !__aarch64__ */

/**
 * @file archcap-assertions.h
 * @brief archcap internals.
 *        Implementation of archcap assertions.
 */

#if !defined(__ARCHCAP_INTERNAL_HEADERS_ALLOWED)
#error "Please, include <archcap.h> instead"
#endif /* !__ARCHCAP_INTERNAL_HEADERS_ALLOWED */

#include <stddef.h>

/* Internal: run-time assertions */
#if defined(ARCHCAP_NO_RUNTIME_ASSERTIONS)
#define __ARCHCAP_ASSERT(condition)                                          \
    __extension__({                                                          \
        if (false) {                                                         \
            (void) (condition);                                              \
        }                                                                    \
    })
#else /* !ARCHCAP_NO_RUNTIME_ASSERTIONS */
#if __ARCHCAP_PRINT_SUPPORTED && ARCHCAP_CAPABILITIES_AVAILABLE
#define __ARCHCAP_PRINT_MESSAGE(message)                                     \
    __extension__({                                                          \
        register int __x8 asm("x8") = __NR_write;                            \
        register int __x0 asm("x0") = STDERR_FILENO;                         \
        /* For purecap, this should be a capability register,                \
         * which is required by the compiler.                                \
         * For hybrid, this doesn't make a difference.                       \
         * The kernel, according to the current ABI, would anyway            \
         * only look into the address part. */                               \
        register const char* __x1 asm("c1") = message;                       \
        register size_t __x2 asm("x2") = sizeof(message);                    \
        asm volatile("svc #0\r\n"                                            \
                     : "+r"(__x0)                                            \
                     : "r" (__x1), "r" (__x2), "r"(__x8));                   \
    })
#else /* !__ARCHCAP_PRINT_SUPPORTED || !ARCHCAP_CAPABILITIES_AVAILABLE */
#define __ARCHCAP_PRINT_MESSAGE(message)                                     \
    __extension__({                                                          \
        (void) (message);                                                    \
        __builtin_trap();                                                    \
    })
#endif /* !__ARCHCAP_PRINT_SUPPORTED || !ARCHCAP_CAPABILITIES_AVAILABLE */

#define __ARCHCAP_ASSERT_STRINGIFY_FORWARD(operand) #operand
#define __ARCHCAP_ASSERT_STRINGIFY(operand)                                  \
    __ARCHCAP_ASSERT_STRINGIFY_FORWARD(operand)
#define __ARCHCAP_ASSERT(condition)                                          \
    __extension__({                                                          \
        if (!(condition)) {                                                  \
            /* Putting the string on stack to avoid need in relocations      \
             * so that the asserts work before capability relocations        \
             * are processed */                                              \
            const char __message[] =                                         \
                __FILE__ ":"                                                 \
                __ARCHCAP_ASSERT_STRINGIFY(__LINE__)                         \
                "    "                                                       \
                #condition;                                                  \
                                                                             \
            __ARCHCAP_PRINT_MESSAGE(__message);                              \
                                                                             \
            __builtin_trap();                                                \
        }                                                                    \
    })
#endif /* !ARCHCAP_NO_RUNTIME_ASSERTIONS */

/* Internal: compile-time assertions */
#if defined(__cplusplus)
__ARCHCAP_START_CPP_SECTION
#define __ARCHCAP_STATIC_ASSERT(condition)                                   \
    static_assert(condition, #condition)
__ARCHCAP_END_CPP_SECTION
#else /* !__cplusplus */
#define __ARCHCAP_STATIC_ASSERT(condition)                                   \
    __extension__({                                                          \
        char __attribute__((unused)) __array[(condition) ? 1 : -1];          \
    })
#endif /* !__cplusplus */

#if defined(ARCHCAP_NO_RUNTIME_TAG_ASSERTIONS)
#define __ARCHCAP_ASSERT_TAGGED(cap)                                         \
    __extension__({                                                          \
        if (false) {                                                         \
            (void) (cap);                                                    \
        }                                                                    \
    })
#else /* !ARCHCAP_NO_RUNTIME_TAG_ASSERTIONS */
#define __ARCHCAP_ASSERT_TAGGED(cap)                                         \
    __extension__({                                                          \
        __ARCHCAP_ASSERT(archcap_c_tag_get(cap));                            \
    })
#endif /* !ARCHCAP_NO_RUNTIME_TAG_ASSERTIONS */
