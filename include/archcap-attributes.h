/*
 * Copyright (c) 2020 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include "archcap-defs.h"

#include <stdint.h>

/**
 * @file archcap-attributes.h
 * @brief Defines part of the external archcap interface.
 *        Implementation of archcap attribute helpers.
 */

/* Interface: conditional structure packing */
#if ARCHCAP_ABI_PURECAP
# define ARCHCAP_PACKED_IF_NOT_PURECAP_ABI
#else /* !ARCHCAP_ABI_PURECAP */
# define ARCHCAP_PACKED_IF_NOT_PURECAP_ABI __attribute__((__packed__))
#endif /* !ARCHCAP_ABI_PURECAP */

/* Interface: conditional alignment to pointer size */
#if ARCHCAP_ABI_PURECAP
# define ARCHCAP_ALIGN_TO_POINTER_IF_PURECAP_ABI \
    __attribute__((aligned(sizeof(uintptr_t))))
#else /* !ARCHCAP_ABI_PURECAP */
# define ARCHCAP_ALIGN_TO_POINTER_IF_PURECAP_ABI
#endif /* !ARCHCAP_ABI_PURECAP */

/* Interface: conditional alignment to pointer size */
#if ARCHCAP_ABI_PURECAP
# define ARCHCAP_ALIGNAS_POINTER_IF_PURECAP_ABI(align) \
    alignas(align) alignas(sizeof(uintptr_t))
#else /* !ARCHCAP_ABI_PURECAP */
# define ARCHCAP_ALIGNAS_POINTER_IF_PURECAP_ABI(align) \
    alignas(align)
#endif /* !ARCHCAP_ABI_PURECAP */
