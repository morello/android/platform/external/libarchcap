/*
 * Copyright (c) 2020 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

/**
 * @file archcap-casts.h
 * @brief archcap internals.
 *        Implementation of archcap type casts.
 */

#if !defined(__ARCHCAP_INTERNAL_HEADERS_ALLOWED)
#error "Please, include <archcap.h> instead"
#endif /* !__ARCHCAP_INTERNAL_HEADERS_ALLOWED */

#include <stddef.h>
#include <stdint.h>

/* Internal: casts */
#if defined(__cplusplus)
__ARCHCAP_START_CPP_SECTION
template<typename T, typename U>
static inline __attribute__((__always_inline__)) T
__archcap_cast_helper(U expr_value) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wold-style-cast"
    return (T) expr_value;
#pragma clang diagnostic pop
}

#define __ARCHCAP_CAST(type, expression)                                     \
    __extension__({                                                          \
        __ARCHCAP_STATIC_ASSERT(sizeof(type) == sizeof(expression));         \
        __archcap_cast_helper<type>(expression);                             \
    })

#define __ARCHCAP_CAST_CAN_TRUNC(type, expression)                           \
    __extension__({                                                          \
        __archcap_cast_helper<type>(expression);                             \
    })
__ARCHCAP_END_CPP_SECTION
#else /* !__cplusplus */
#define __ARCHCAP_CAST(type, expression)                                     \
    __extension__({                                                          \
        __ARCHCAP_STATIC_ASSERT(sizeof(type) == sizeof(expression));         \
        ((type) (expression));                                               \
    })
#define __ARCHCAP_CAST_CAN_TRUNC(type, expression)                           \
    ((type) (expression))
#endif /* !__cplusplus */

#define __ARCHCAP_CAST_UINTCAP(expression)                                   \
    __ARCHCAP_CAST(uintcap_t, expression)
#define __ARCHCAP_CAST_CONST_VOID_CAP(expression)                            \
    __ARCHCAP_CAST(const void* __capability, expression)
#define __ARCHCAP_CAST_UINTPTR(expression)                                   \
    __ARCHCAP_CAST(uintptr_t, expression)
#define __ARCHCAP_CAST_SIZE(expression)                                      \
    __ARCHCAP_CAST_CAN_TRUNC(size_t, expression)
#define __ARCHCAP_CAST_PTRADDR(expression)                                   \
    __ARCHCAP_CAST_CAN_TRUNC(__PTRADDR_TYPE__, expression)
