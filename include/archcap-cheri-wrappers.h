/*
 * Copyright (c) 2020 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

/**
 * @file archcap-cheri-wrappers.h
 * @brief archcap internals.
 *
 *        Private wrappers for the cheri_* macros that take a capability as
 *        `void* __capability`.
 *        The wrappers accept any capability type, and return the same type
 *        (where relevant).
 */

#if !defined(__ARCHCAP_INTERNAL_HEADERS_ALLOWED)
#error "Please, include <archcap.h> instead"
#endif /* !__ARCHCAP_INTERNAL_HEADERS_ALLOWED */

#define __ARCHCAP_CHERI_ADDRESS_GET(cap)                                     \
    cheri_address_get(__ARCHCAP_CAST_CONST_VOID_CAP(cap))

#define __ARCHCAP_CHERI_ADDRESS_SET(cap, value)                              \
    __ARCHCAP_CAST(__typeof__(cap), cheri_address_set(                       \
            __ARCHCAP_CAST_CONST_VOID_CAP(cap), value))

#define __ARCHCAP_CHERI_COPY_FROM_HIGH(cap)                                  \
    __builtin_cheri_copy_from_high(__ARCHCAP_CAST_CONST_VOID_CAP(cap))

#define __ARCHCAP_CHERI_BASE_GET(cap)                                        \
    cheri_base_get(__ARCHCAP_CAST_CONST_VOID_CAP(cap))

#define __ARCHCAP_CHERI_BOUNDS_SET(cap, value)                               \
    __ARCHCAP_CAST(__typeof__(cap), cheri_bounds_set(                        \
            __ARCHCAP_CAST_CONST_VOID_CAP(cap), value))

#define __ARCHCAP_CHERI_BOUNDS_SET_EXACT(cap, value)                         \
    __ARCHCAP_CAST(__typeof__(cap), cheri_bounds_set_exact(                  \
            __ARCHCAP_CAST_CONST_VOID_CAP(cap), value))

#define __ARCHCAP_CHERI_CAP_BUILD(key, cap)                                  \
    __ARCHCAP_CAST(__typeof__(cap), cheri_cap_build(                         \
            __ARCHCAP_CAST_CONST_VOID_CAP(key),                              \
            __ARCHCAP_CAST_UINTCAP(cap)))

#define __ARCHCAP_CHERI_LENGTH_GET(cap)                                      \
    cheri_length_get(__ARCHCAP_CAST_CONST_VOID_CAP(cap))

#define __ARCHCAP_CHERI_PERMS_GET(cap)                                       \
    cheri_perms_get(__ARCHCAP_CAST_CONST_VOID_CAP(cap))

#define __ARCHCAP_CHERI_PERMS_CLEAR(cap, perms)                              \
    __ARCHCAP_CAST(__typeof__(cap), cheri_perms_clear(                       \
            __ARCHCAP_CAST_CONST_VOID_CAP(cap), perms))

#define __ARCHCAP_CHERI_TAG_GET(cap)                                         \
    cheri_tag_get(__ARCHCAP_CAST_CONST_VOID_CAP(cap))

#define __ARCHCAP_CHERI_SEAL(cap, cap_key)                                   \
    __ARCHCAP_CAST(__typeof__(cap), cheri_seal(                              \
            __ARCHCAP_CAST_CONST_VOID_CAP(cap),                              \
            __ARCHCAP_CAST_CONST_VOID_CAP(cap_key)))

#define __ARCHCAP_CHERI_UNSEAL(cap, cap_key)                                 \
    __ARCHCAP_CAST(__typeof__(cap), cheri_unseal(                            \
            __ARCHCAP_CAST_CONST_VOID_CAP(cap),                              \
            __ARCHCAP_CAST_CONST_VOID_CAP(cap_key)))

#define __ARCHCAP_CHERI_IS_SEALED(cap)                                       \
    cheri_is_sealed(__ARCHCAP_CAST_CONST_VOID_CAP(cap))

#define __ARCHCAP_CHERI_IS_UNSEALED(cap)                                     \
    cheri_is_unsealed(__ARCHCAP_CAST_CONST_VOID_CAP(cap))

#define __ARCHCAP_CHERI_SENTRY_CREATE(cap)                                   \
    __ARCHCAP_CAST(__typeof__(cap), cheri_sentry_create(                     \
            __ARCHCAP_CAST_CONST_VOID_CAP(cap)))

#define __ARCHCAP_CHERI_IS_SENTRY(cap)                                       \
    cheri_is_sentry(__ARCHCAP_CAST_CONST_VOID_CAP(cap))
