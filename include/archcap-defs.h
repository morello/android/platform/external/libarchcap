/*
 * Copyright (c) 2020 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

/**
 * @file archcap-defs.h
 * @brief Defines part of the external archcap interface.
 *        Types, constants, preprocessor guards.
 */

#if defined(__CHERI__)
#include <cheriintrin.h>
#endif

#define __ARCHCAP_INTERNAL_HEADERS_ALLOWED

#include "archcap-asm.h"

#undef __ARCHCAP_INTERNAL_HEADERS_ALLOWED

typedef int archcap_perms_t;
