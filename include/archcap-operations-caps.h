/*
 * Copyright (c) 2020 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

/**
 * @file archcap-caps.h
 * @brief Defines part of the external archcap interface.
 *
 *        The helpers operate on capabilities - therefore suitable for
 *        pointers in the pure capability ABI, however not in the hybrid
 *        ABI where pointer is not the same as capability.
 */

#if !defined(__ARCHCAP_INTERNAL_HEADERS_ALLOWED)
#error "Please, include <archcap.h> instead"
#endif /* !__ARCHCAP_INTERNAL_HEADERS_ALLOWED */

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "archcap-cheri-wrappers.h"

/* Interface: capability operations */
#define archcap_c_tag_get(cap)                                               \
    __ARCHCAP_CHERI_TAG_GET(cap)

#define archcap_c_seal(cap, cap_key)                                         \
    __ARCHCAP_CHERI_SEAL(cap, cap_key)

#define archcap_c_unseal(cap, cap_key)                                       \
    __ARCHCAP_CHERI_UNSEAL(cap, cap_key)

#define archcap_c_is_sealed(cap)                                             \
    __ARCHCAP_CHERI_IS_SEALED(cap)

#define archcap_c_is_unsealed(cap)                                           \
    __ARCHCAP_CHERI_IS_UNSEALED(cap)

#define archcap_c_sentry_create(cap)                                         \
    __ARCHCAP_CHERI_SENTRY_CREATE(cap)

#define archcap_c_is_sentry(cap)                                             \
    __ARCHCAP_CHERI_IS_SENTRY(cap)

#define archcap_c_address_get(cap)                                           \
    __ARCHCAP_CHERI_ADDRESS_GET(cap)

#define archcap_c_address_signext55_get(cap)                                 \
    __extension__({                                                          \
        uintcap_t __cap0 = __ARCHCAP_CAST_UINTCAP(cap);                      \
        ptraddr_t __cap_ptr_value0 = archcap_c_address_get(__cap0);          \
        ((__cap_ptr_value0 >> 55) & 1 ? __cap_ptr_value0 | (0xFFUL << 56)    \
                                      : __cap_ptr_value0 & ~(0xFFUL << 56)); \
     })

#define archcap_c_address_set(cap, value)                                    \
    __extension__({                                                          \
        uintcap_t __cap0 = __ARCHCAP_CAST_UINTCAP(cap);                      \
        __PTRADDR_TYPE__ __value_sz0 = __ARCHCAP_CAST_PTRADDR(value);        \
                                                                             \
        bool __is_tagged_before0 = archcap_c_tag_get(__cap0);                \
                                                                             \
        __cap0 = __ARCHCAP_CHERI_ADDRESS_SET(__cap0, __value_sz0);           \
                                                                             \
        bool __is_tagged_after0 = archcap_c_tag_get(__cap0);                 \
        __ARCHCAP_ASSERT(__is_tagged_before0 == __is_tagged_after0);         \
                                                                             \
        __ARCHCAP_CAST(__typeof__(cap), __cap0);                             \
    })

#define __ARCHCAP_VALUE_BINARY_OP(cap, value, op)                            \
    __extension__({                                                          \
        uintcap_t __cap1 = __ARCHCAP_CAST_UINTCAP(cap);                      \
        __typeof__(value) __value1 = (value);                                \
                                                                             \
        __cap1 = archcap_c_address_set(__cap1,                               \
            archcap_c_address_get(__cap1) op (__value1));                    \
                                                                             \
        __ARCHCAP_CAST(__typeof__(cap), __cap1);                             \
    })

#define archcap_c_address_add(cap, value)                                    \
    __ARCHCAP_VALUE_BINARY_OP(cap, value, +)

#define archcap_c_address_sub(cap, value)                                    \
    __ARCHCAP_VALUE_BINARY_OP(cap, value, -)

#define archcap_c_address_and(cap, mask)                                     \
    __ARCHCAP_VALUE_BINARY_OP(cap, mask, &)

#define archcap_c_address_or(cap, mask)                                      \
    __ARCHCAP_VALUE_BINARY_OP(cap, mask, |)

#define archcap_c_address_xor(cap, mask)                                     \
    __ARCHCAP_VALUE_BINARY_OP(cap, mask, ^)

#define __ARCHCAP_VALUE_ALIGN_OP(cap, alignment, op)                         \
    __extension__({                                                          \
        uintcap_t __cap1 = __ARCHCAP_CAST_UINTCAP(cap);                      \
        __typeof__(alignment) __alignment = (alignment);                     \
                                                                             \
        bool __is_tagged_before1 = archcap_c_tag_get(__cap1);                \
                                                                             \
        __cap1 = cheri_align_ ## op(__cap1, __alignment);                    \
                                                                             \
        bool __is_tagged_after1 = archcap_c_tag_get(__cap1);                 \
        __ARCHCAP_ASSERT(__is_tagged_before1 == __is_tagged_after1);         \
                                                                             \
        __ARCHCAP_CAST(__typeof__(cap), __cap1);                             \
    })

#define archcap_c_address_align_down(cap, alignment)                         \
    __ARCHCAP_VALUE_ALIGN_OP(cap, alignment, down)

#define archcap_c_address_align_up(cap, alignment)                           \
    __ARCHCAP_VALUE_ALIGN_OP(cap, alignment, up)

#define archcap_c_address_diff(cap1, cap2)                                   \
    (archcap_c_address_get(cap1) - archcap_c_address_get(cap2))
#define archcap_c_address_clear_bits(cap, mask)                              \
    archcap_c_address_and(cap, ~(mask))
#define archcap_c_address_get_bits(cap, mask)                                \
    (archcap_c_address_get(cap) & (mask))

#define archcap_c_metadata_get(cap)                                          \
    __ARCHCAP_CHERI_COPY_FROM_HIGH(cap)

#define archcap_c_is_same(cap1, cap2)                                        \
    __extension__({                                                          \
        uintcap_t __cap1 = __ARCHCAP_CAST_UINTCAP(cap1);                     \
        uintcap_t __cap2 = __ARCHCAP_CAST_UINTCAP(cap2);                     \
                                                                             \
        (archcap_c_address_get(__cap1) == archcap_c_address_get(__cap2) &&   \
         archcap_c_metadata_get(__cap1) == archcap_c_metadata_get(__cap2) && \
         archcap_c_tag_get(__cap1) == archcap_c_tag_get(__cap2));            \
    })

#define archcap_c_perms_get(cap)                                             \
    __extension__({                                                          \
        uintcap_t __cap0 = __ARCHCAP_CAST_UINTCAP(cap);                      \
                                                                             \
        __ARCHCAP_ASSERT_TAGGED(__cap0);                                     \
                                                                             \
        __ARCHCAP_CHERI_PERMS_GET(__cap0);                                   \
    })

#define archcap_c_perms_clear(cap, perms)                                    \
    __extension__({                                                          \
        uintcap_t __cap1 = __ARCHCAP_CAST_UINTCAP(cap);                      \
        archcap_perms_t __perms1 = (perms);                                  \
                                                                             \
        bool __is_tagged_before1 = archcap_c_tag_get(__cap1);                \
        __ARCHCAP_ASSERT(__perms1 == (__perms1 & ARCHCAP_PERM_MASK_ALL));    \
                                                                             \
        __cap1 = __ARCHCAP_CHERI_PERMS_CLEAR(__cap1, __perms1);              \
                                                                             \
        bool __is_tagged_after1 = archcap_c_tag_get(__cap1);                 \
        __ARCHCAP_ASSERT(__is_tagged_before1 == __is_tagged_after1);         \
        __ARCHCAP_ASSERT((archcap_c_perms_get(__cap1) & __perms1) == 0);     \
                                                                             \
        __ARCHCAP_CAST(__typeof__(cap), __cap1);                             \
    })

#define archcap_c_perms_and(cap, perms)                                      \
    __extension__({                                                          \
        uintcap_t __cap2 = __ARCHCAP_CAST_UINTCAP(cap);                      \
        archcap_perms_t __perms2 = (perms);                                  \
                                                                             \
        __ARCHCAP_ASSERT(__perms2 == (__perms2 & ARCHCAP_PERM_MASK_ALL));    \
                                                                             \
        __cap2 = archcap_c_perms_clear(__cap2,                               \
            ARCHCAP_PERM_MASK_ALL & (~__perms2));                            \
                                                                             \
        __ARCHCAP_CAST(__typeof__(cap), __cap2);                             \
    })

#define archcap_c_perms_set(cap, perms)                                      \
    __extension__({                                                          \
        uintcap_t __cap2 = __ARCHCAP_CAST_UINTCAP(cap);                      \
        archcap_perms_t __perms2 = (perms);                                  \
        archcap_perms_t __perms_prev2 = archcap_c_perms_get(__cap2);         \
                                                                             \
        bool __is_tagged_before2 = archcap_c_tag_get(__cap2);                \
        __ARCHCAP_ASSERT(__perms2 == (__perms2 & ARCHCAP_PERM_MASK_ALL));    \
        __ARCHCAP_ASSERT((__perms2 & __perms_prev2) == __perms2);            \
                                                                             \
        __cap2 = archcap_c_perms_clear(__cap2, __perms_prev2 & ~__perms2);   \
                                                                             \
        bool __is_tagged_after2 = archcap_c_tag_get(__cap2);                 \
        __ARCHCAP_ASSERT(__is_tagged_before2 == __is_tagged_after2);         \
        __ARCHCAP_ASSERT(archcap_c_perms_get(__cap2) == __perms2);           \
                                                                             \
        __ARCHCAP_CAST(__typeof__(cap), __cap2);                             \
    })

#define archcap_c_perms_has(cap, perms)                                      \
    __extension__({                                                          \
        uintcap_t __cap1 = __ARCHCAP_CAST_UINTCAP(cap);                      \
        archcap_perms_t __perms1 = (perms);                                  \
                                                                             \
        ((archcap_c_perms_get(__cap1) & __perms1) == __perms1);              \
    })

/* Permissions: Load */
#define archcap_c_perms_has_load(cap)                                        \
    archcap_c_perms_has(cap, ARCHCAP_PERM_LOAD)
#define archcap_c_perms_has_load_cap(cap)                                    \
    archcap_c_perms_has(cap, ARCHCAP_PERM_LOAD_CAP)

/* Permissions: Store */
#define archcap_c_perms_has_store(cap)                                       \
    archcap_c_perms_has(cap, ARCHCAP_PERM_STORE)
#define archcap_c_perms_has_store_cap(cap)                                   \
    archcap_c_perms_has(cap, ARCHCAP_PERM_STORE_CAP)
#define archcap_c_perms_has_store_local_cap(cap)                             \
    archcap_c_perms_has(cap, ARCHCAP_PERM_STORE_LOCAL_CAP)

/* Permissions: Execute */
#define archcap_c_perms_has_execute(cap)                                     \
    archcap_c_perms_has(cap, ARCHCAP_PERM_EXECUTE)

/* Permissions: Load capability with write permissions */
#define archcap_c_perms_has_mutable_load(cap)                                \
    archcap_c_perms_has(cap, ARCHCAP_PERM_MORELLO_MUTABLE_LOAD)

/* Permissions: Can be stored as a global capability */
#define archcap_c_perms_has_global(cap)                                      \
    archcap_c_perms_has(cap, ARCHCAP_PERM_GLOBAL)

/* Permissions: Is a compartment ID */
#define archcap_c_perms_has_compartment_id(cap)                              \
    archcap_c_perms_has(cap, ARCHCAP_PERM_MORELLO_COMPARTMENT_ID)

/* Permissions: Non-restricted execution mode */
#define archcap_c_perms_has_executive(cap)                                   \
    archcap_c_perms_has(cap, ARCHCAP_PERM_MORELLO_EXECUTIVE)

/* Permissions: Access to system registers */
#define archcap_c_perms_has_system(cap)                                      \
    archcap_c_perms_has(cap, ARCHCAP_PERM_SYSTEM)

/* Permissions: Sealing */
#define archcap_c_perms_has_seal(cap)                                        \
    archcap_c_perms_has(cap, ARCHCAP_PERM_SEAL)
#define archcap_c_perms_has_unseal(cap)                                      \
    archcap_c_perms_has(cap, ARCHCAP_PERM_UNSEAL)
#define archcap_c_perms_has_branch_sealed_pair(cap)                          \
    archcap_c_perms_has(cap, ARCHCAP_PERM_MORELLO_BRANCH_SEALED_PAIR)

/* Bounds */
#define archcap_c_base_get(cap)                                              \
    __extension__({                                                          \
        uintcap_t __cap0 = __ARCHCAP_CAST_UINTCAP(cap);                      \
                                                                             \
        __ARCHCAP_ASSERT_TAGGED(__cap0);                                     \
                                                                             \
        __ARCHCAP_CHERI_BASE_GET(__cap0);                                    \
    })

#define archcap_c_limit_get(cap)                                             \
    __extension__({                                                          \
        uintcap_t __cap0 = __ARCHCAP_CAST_UINTCAP(cap);                      \
                                                                             \
        __ARCHCAP_ASSERT_TAGGED(__cap0);                                     \
                                                                             \
        /* Some combinations of base and length are each                     \
         * representable using ptraddr_t, but base + length                  \
         * overflows (in which case base + length is always                  \
         * 2^64). In this case we need to catch the overflow                 \
         * and return 2^64 - 1 as the limit instead. */                      \
        ptraddr_t __sum0;                                                    \
        __builtin_add_overflow(                                              \
            __ARCHCAP_CHERI_BASE_GET(__cap0),                                \
            __ARCHCAP_CHERI_LENGTH_GET(__cap0),                              \
            &__sum0) ? __ARCHCAP_CAST_PTRADDR(~0UL) : __sum0;                \
    })

#define archcap_c_length_get(cap)                                            \
    __extension__({                                                          \
        uintcap_t __cap0 = __ARCHCAP_CAST_UINTCAP(cap);                      \
                                                                             \
        __ARCHCAP_ASSERT_TAGGED(__cap0);                                     \
                                                                             \
        __ARCHCAP_CHERI_LENGTH_GET(__cap0);                                  \
    })

#define archcap_c_is_in_bounds(cap, size)                                    \
    __extension__({                                                          \
        uintcap_t __cap1 = __ARCHCAP_CAST_UINTCAP(cap);                      \
        size_t __size_sz1 = __ARCHCAP_CAST_SIZE(size);                       \
        ptraddr_t __cap_sxt55value1;                                         \
        __cap_sxt55value1 = archcap_c_address_signext55_get(__cap1);         \
                                                                             \
        ptraddr_t __sum1;                                                    \
        bool __is_overflow1 = __builtin_add_overflow(__cap_sxt55value1,      \
                                                     __size_sz1,             \
                                                     &__sum1);               \
                                                                             \
        (!__is_overflow1 &&                                                  \
         (archcap_c_base_get(__cap1) <= __cap_sxt55value1) &&                \
         (archcap_c_limit_get(__cap1) >= __sum1));                           \
    })

#define __ARCHCAP_BOUNDS_SET_POST_ASSERT_SET(cap, value, base, length)       \
    __extension__({                                                          \
        __ARCHCAP_ASSERT(archcap_c_address_get(cap) == (value));             \
        __ARCHCAP_ASSERT(archcap_c_base_get(cap) <= (base));                 \
        __ARCHCAP_ASSERT(archcap_c_length_get(cap) >= (length));             \
    })

#define __ARCHCAP_BOUNDS_SET_POST_ASSERT_SET_EXACT(cap, value, base, length) \
    __extension__({                                                          \
        __ARCHCAP_ASSERT(archcap_c_address_get(cap) == (value));             \
        __ARCHCAP_ASSERT(archcap_c_base_get(cap) == (base));                 \
        __ARCHCAP_ASSERT(archcap_c_length_get(cap) == (length));             \
    })

#define __ARCHCAP_BOUNDS_SET_LENGTH(cap, length, type)                       \
    __extension__({                                                          \
        uintcap_t __cap4 = __ARCHCAP_CAST_UINTCAP(cap);                      \
        __PTRADDR_TYPE__ __base_sz4 = archcap_c_address_get(__cap4);         \
        size_t __length_sz4 = __ARCHCAP_CAST_SIZE(length);                   \
                                                                             \
        bool __is_tagged_before4 = archcap_c_tag_get(__cap4);                \
                                                                             \
        __cap4 = __ARCHCAP_CHERI_BOUNDS_ ## type(__cap4, __length_sz4);      \
                                                                             \
        bool __is_tagged_after4 = archcap_c_tag_get(__cap4);                 \
        __ARCHCAP_ASSERT(__is_tagged_before4 == __is_tagged_after4);         \
        __ARCHCAP_BOUNDS_SET_POST_ASSERT_ ## type(__cap4,                    \
            __base_sz4, __base_sz4, __length_sz4);                           \
                                                                             \
        __ARCHCAP_CAST(__typeof__(cap), __cap4);                             \
    })

#define __ARCHCAP_BOUNDS_SET_BASE_LENGTH(cap, base, length, type)            \
    __extension__({                                                          \
        uintcap_t __cap4 = __ARCHCAP_CAST_UINTCAP(cap);                      \
        __PTRADDR_TYPE__ __base_sz4 = __ARCHCAP_CAST_PTRADDR(base);          \
        size_t __length_sz4 = __ARCHCAP_CAST_SIZE(length);                   \
        __PTRADDR_TYPE__ __cap_ptr_value4 = archcap_c_address_get(__cap4);   \
                                                                             \
        bool __is_tagged_before4 = archcap_c_tag_get(__cap4);                \
                                                                             \
        __cap4 = archcap_c_address_set(__cap4, __base_sz4);                  \
        __cap4 = __ARCHCAP_CHERI_BOUNDS_ ## type(__cap4, __length_sz4);      \
        __cap4 = archcap_c_address_set(__cap4, __cap_ptr_value4);            \
                                                                             \
        bool __is_tagged_after4 = archcap_c_tag_get(__cap4);                 \
        __ARCHCAP_ASSERT(__is_tagged_before4 == __is_tagged_after4);         \
        __ARCHCAP_BOUNDS_SET_POST_ASSERT_ ## type(__cap4,                    \
            __cap_ptr_value4, __base_sz4, __length_sz4);                     \
                                                                             \
        __ARCHCAP_CAST(__typeof__(cap), __cap4);                             \
    })

#define __ARCHCAP_BOUNDS_SET_BASE_LIMIT(cap, base, limit, type)              \
    __extension__({                                                          \
        size_t __length_sz5 = __ARCHCAP_CAST_PTRADDR(limit) -                \
            __ARCHCAP_CAST_PTRADDR(base);                                    \
                                                                             \
        __ARCHCAP_BOUNDS_SET_BASE_LENGTH(cap, base, __length_sz5, type);     \
    })

#define archcap_c_representable_length(length)                               \
    cheri_representable_length(length)

#define archcap_c_representable_alignment_mask(length)                       \
    cheri_representable_alignment_mask(length)

#define archcap_c_bounds_set(cap, length)                                    \
    __ARCHCAP_BOUNDS_SET_LENGTH(cap, length, SET)

#define archcap_c_bounds_set_exact(cap, length)                              \
    __ARCHCAP_BOUNDS_SET_LENGTH(cap, length, SET_EXACT)

#define archcap_c_bounds_set_base_length(cap, base, length)                  \
    __ARCHCAP_BOUNDS_SET_BASE_LENGTH(cap, base, length, SET)

#define archcap_c_bounds_set_exact_base_length(cap, base, length)            \
    __ARCHCAP_BOUNDS_SET_BASE_LENGTH(cap, base, length, SET_EXACT)

#define archcap_c_bounds_set_base_limit(cap, base, limit)                    \
    __ARCHCAP_BOUNDS_SET_BASE_LIMIT(cap, base, limit, SET)

#define archcap_c_bounds_set_exact_base_limit(cap, base, limit)              \
    __ARCHCAP_BOUNDS_SET_BASE_LIMIT(cap, base, limit, SET_EXACT)

/* Set bounds according to the type with base at the current value
 * Bounds can be rounded outwards if otherwise non-representable */
#define archcap_c_bounds_set_from_type(T, cap)                               \
    __extension__({                                                          \
        uintcap_t __cap3 = __ARCHCAP_CAST_UINTCAP(cap);                      \
                                                                             \
        __cap3 = archcap_c_bounds_set(__cap3, sizeof(T));                    \
                                                                             \
        __ARCHCAP_CAST(T* __capability, __cap3);                             \
    })

/* Set bounds according to the type with base at the current value.
 * Bounds can't be rounded even if non-representable - otherwise,
 * the capability tag is cleared */
#define archcap_c_bounds_set_exact_from_type(T, cap)                         \
    __extension__({                                                          \
        uintcap_t __cap3 = __ARCHCAP_CAST_UINTCAP(cap);                      \
                                                                             \
        __cap3 = archcap_c_bounds_set_exact(__cap3, sizeof(T));              \
                                                                             \
        __ARCHCAP_CAST(T* __capability, __cap3);                             \
    })

#if defined(__cplusplus)
__ARCHCAP_START_CPP_SECTION
template<typename T, typename U>
T* __capability archcap_c_cast(U cap) {
    return archcap_c_bounds_set_exact_from_type(T, cap);
}
__ARCHCAP_END_CPP_SECTION
#endif /* __cplusplus */

/* Create a valid capability from an untagged capability and a (valid) key.
 * It must be possible to derive cap from key. The returned capability is equal
 * to cap with its tag set. */
#define archcap_c_cap_build(key, cap)                                        \
    __extension__({                                                          \
        uintcap_t __key1 = __ARCHCAP_CAST_UINTCAP(key);                      \
        uintcap_t __cap1 = __ARCHCAP_CAST_UINTCAP(cap);                      \
                                                                             \
        __ARCHCAP_ASSERT_TAGGED(__key1);                                     \
                                                                             \
        __cap1 = __ARCHCAP_CHERI_CAP_BUILD(__key1, __cap1);                  \
                                                                             \
        __ARCHCAP_ASSERT_TAGGED(__cap1);                                     \
                                                                             \
        __ARCHCAP_CAST(__typeof__(cap), __cap1);                             \
    })

/* DDC */
#define archcap_c_ddc_get()                                                  \
    __ARCHCAP_CAST_UINTCAP(cheri_ddc_get())
#define archcap_c_from_ddc(value)                                            \
    archcap_c_address_set(archcap_c_ddc_get(), value)
#define archcap_c_from_ddc_perms_clear(value, perms)                         \
    archcap_c_perms_clear(archcap_c_from_ddc(value), perms)
#define archcap_c_from_ddc_bounded_perms_clear(value, length, perms)         \
    archcap_c_bounds_set(archcap_c_from_ddc_perms_clear(value, perms),       \
        length)
#define archcap_c_from_ddc_exactly_bounded_perms_clear(value, length, perms) \
    archcap_c_bounds_set_exact(archcap_c_from_ddc_perms_clear(value, perms), \
        length)

#define archcap_c_from_ddc_clear_x(value)                                    \
    archcap_c_from_ddc_perms_clear(value,                                    \
        ARCHCAP_PERM_EXECUTE)
#define archcap_c_from_ddc_clear_w(value)                                    \
    archcap_c_from_ddc_perms_clear(value,                                    \
        ARCHCAP_PERM_STORE |                                                 \
        ARCHCAP_PERM_STORE_CAP |                                             \
        ARCHCAP_PERM_STORE_LOCAL_CAP)
#define archcap_c_from_ddc_clear_wx(value)                                   \
    archcap_c_from_ddc_perms_clear(value,                                    \
        ARCHCAP_PERM_EXECUTE |                                               \
        ARCHCAP_PERM_STORE |                                                 \
        ARCHCAP_PERM_STORE_CAP |                                             \
        ARCHCAP_PERM_STORE_LOCAL_CAP)
#define archcap_c_from_ddc_clear_r(value)                                    \
    archcap_c_from_ddc_perms_clear(value,                                    \
        ARCHCAP_PERM_LOAD |                                                  \
        ARCHCAP_PERM_LOAD_CAP)

#define archcap_c_from_ddc_bounded(value, length)                            \
    archcap_c_from_ddc_bounded_perms_clear(value, length, 0)

#define archcap_c_from_ddc_bounded_clear_x(value, length)                    \
    archcap_c_from_ddc_bounded_perms_clear(value, length,                    \
        ARCHCAP_PERM_EXECUTE)
#define archcap_c_from_ddc_bounded_clear_w(value, length)                    \
    archcap_c_from_ddc_bounded_perms_clear(value, length,                    \
        ARCHCAP_PERM_STORE |                                                 \
        ARCHCAP_PERM_STORE_CAP |                                             \
        ARCHCAP_PERM_STORE_LOCAL_CAP)
#define archcap_c_from_ddc_bounded_clear_wx(value, length)                   \
    archcap_c_from_ddc_bounded_perms_clear(value, length,                    \
        ARCHCAP_PERM_EXECUTE |                                               \
        ARCHCAP_PERM_STORE |                                                 \
        ARCHCAP_PERM_STORE_CAP |                                             \
        ARCHCAP_PERM_STORE_LOCAL_CAP)
#define archcap_c_from_ddc_bounded_clear_r(value, length)                    \
    archcap_c_from_ddc_bounded_perms_clear(value, length,                    \
        ARCHCAP_PERM_LOAD |                                                  \
        ARCHCAP_PERM_LOAD_CAP)

/* Create capability from DDC.
 * Set bounds according to the type with base at the current value.
 * Bounds can't be rounded even if non-representable - otherwise,
 * the capability tag is cleared */
#define archcap_c_from_ddc_bounded_from_type(T, value, perms_to_clear)       \
    __extension__({                                                          \
        uintcap_t __cap6 = archcap_c_from_ddc_exactly_bounded_perms_clear(   \
            value, sizeof(T), perms_to_clear);                               \
                                                                             \
        __ARCHCAP_CAST(T* __capability, __cap6);                             \
    })

#if defined(__cplusplus)
__ARCHCAP_START_CPP_SECTION
template<typename T, typename U>
T* __capability
archcap_c_ddc_cast(U value, archcap_perms_t perms_to_clear = 0) {
    return archcap_c_from_ddc_bounded_from_type(T, value, perms_to_clear);
}

#if ARCHCAP_ABI_HYBRID
template<typename T>
T* __capability
archcap_c_ddc_cast(T* value, archcap_perms_t perms_to_clear = 0) {
    return archcap_c_from_ddc_bounded_from_type(T, value, perms_to_clear);
}
#endif /* ARCHCAP_ABI_HYBRID */
__ARCHCAP_END_CPP_SECTION
#endif /* __cplusplus */

/* PCC */
#define archcap_c_pcc_get()                                                  \
    __ARCHCAP_CAST_UINTCAP(cheri_pcc_get())
#define archcap_c_from_pcc(value)                                            \
    archcap_c_address_set(archcap_c_pcc_get(), value)

/* Construction of capabilities which aren't supposed to be dereferenced */
#define archcap_c_nonderef_cast_for_type(T, value)                           \
    __extension__({                                                          \
        uintcap_t __null_cap = 0x0;                                          \
                                                                             \
        __ARCHCAP_CAST(T* __capability,                                      \
            archcap_c_address_set(__null_cap, value));                       \
    })

#if defined(__cplusplus)
__ARCHCAP_START_CPP_SECTION
template<typename T, typename U>
T* __capability archcap_c_nonderef_cast(U value) {
    return archcap_c_nonderef_cast_for_type(T, value);
}
__ARCHCAP_END_CPP_SECTION
#endif /* __cplusplus */
