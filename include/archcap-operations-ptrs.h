/*
 * Copyright (c) 2020 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

/**
 * @file archcap-operation-ptrs.h
 * @brief Defines part of the external archcap interface.
 *
 *        The helpers operate on pointers - therefore suitable
 *        both for pointers in:
 *         - the pure capability ABI where they are capabilities
 *         - the hybrid/base ABI where they are not capabilities
 * */

#if !defined(__ARCHCAP_INTERNAL_HEADERS_ALLOWED)
#error "Please, include <archcap.h> instead"
#endif /* !__ARCHCAP_INTERNAL_HEADERS_ALLOWED */

#include <stddef.h>
#include <stdint.h>

#if ARCHCAP_ABI_PURECAP

#define archcap_address_get(cap)                                             \
    archcap_c_address_get(cap)
#define archcap_address_signext55_get(cap)                                   \
    archcap_c_address_signext55_get(cap)
#define archcap_address_set(cap, value)                                      \
    archcap_c_address_set(cap, value)
#define archcap_address_add(cap, value)                                      \
    archcap_c_address_add(cap, value)
#define archcap_address_sub(cap, value)                                      \
    archcap_c_address_sub(cap, value)
#define archcap_address_and(cap, mask)                                       \
    archcap_c_address_and(cap, mask)
#define archcap_address_or(cap, mask)                                        \
    archcap_c_address_or(cap, mask)
#define archcap_address_xor(cap, mask)                                       \
    archcap_c_address_xor(cap, mask)
#define archcap_address_align_down(cap, alignment)                           \
    archcap_c_address_align_down(cap, alignment)
#define archcap_address_align_up(cap, alignment)                             \
    archcap_c_address_align_up(cap, alignment)
#define archcap_address_diff(cap1, cap2)                                     \
    archcap_c_address_diff(cap1, cap2)
#define archcap_address_clear_bits(cap, mask)                                \
    archcap_c_address_clear_bits(cap, mask)
#define archcap_address_get_bits(cap, mask)                                  \
    archcap_c_address_get_bits(cap, mask)
#define archcap_is_same(cap1, cap2)                                          \
    archcap_c_is_same(cap1, cap2)
#define archcap_perms_get(cap)                                               \
    archcap_c_perms_get(cap)
#define archcap_perms_clear(cap, perms)                                      \
    archcap_c_perms_clear(cap, perms)
#define archcap_perms_and(cap, perms)                                        \
    archcap_c_perms_and(cap, perms)
#define archcap_perms_set(cap, perms)                                        \
    archcap_c_perms_set(cap, perms)
#define archcap_perms_has(cap, perms)                                        \
    archcap_c_perms_has(cap, perms)
#define archcap_base_get(cap)                                                \
    archcap_c_base_get(cap)
#define archcap_limit_get(cap)                                               \
    archcap_c_limit_get(cap)
#define archcap_length_get(cap)                                              \
    archcap_c_length_get(cap)
#define archcap_is_in_bounds(cap, size)                                      \
    archcap_c_is_in_bounds(cap, size)
#define archcap_representable_length(length)                                 \
    archcap_c_representable_length(length)
#define archcap_representable_alignment_mask(length)                         \
    archcap_c_representable_alignment_mask(length)
#define archcap_bounds_set(cap, length)                                      \
    archcap_c_bounds_set(cap, length)
#define archcap_bounds_set_exact(cap, length)                                \
    archcap_c_bounds_set_exact(cap, length)
#define archcap_bounds_set_base_length(cap, base, length)                    \
    archcap_c_bounds_set_base_length(cap, base, length)
#define archcap_bounds_set_exact_base_length(cap, base, length)              \
    archcap_c_bounds_set_exact_base_length(cap, base, length)
#define archcap_bounds_set_base_limit(cap, base, limit)                      \
    archcap_c_bounds_set_base_limit(cap, base, limit)
#define archcap_bounds_set_exact_base_limit(cap, base, limit)                \
    archcap_c_bounds_set_exact_base_limit(cap, base, limit)
#define archcap_cap_build(cap, key)                                          \
    archcap_c_cap_build(cap, key)

#else /* !ARCHCAP_ABI_PURECAP */

/* Interface: alternative implementations for non-capability pointers */
#define archcap_address_get(cap)                                             \
    __ARCHCAP_CAST_UINTPTR(cap)
#define archcap_address_signext55_get(cap)                                   \
    __extension__({                                                          \
        ptraddr_t __cap_ptr_value0 = __ARCHCAP_CAST_PTRADDR(cap);            \
        ((__cap_ptr_value0 >> 55) & 1 ? __cap_ptr_value0 | (0xFFUL << 56)    \
                                      : __cap_ptr_value0 & ~(0xFFUL << 56)); \
     })
#define archcap_address_set(cap, value)                                      \
    __ARCHCAP_CAST(__typeof__(cap), value)
#define archcap_address_add(cap, value)                                      \
    __ARCHCAP_CAST(__typeof__(cap), __ARCHCAP_CAST_PTRADDR(cap) + (value))
#define archcap_address_sub(cap, value)                                      \
    __ARCHCAP_CAST(__typeof__(cap), __ARCHCAP_CAST_PTRADDR(cap) - (value))
#define archcap_address_and(cap, mask)                                       \
    __ARCHCAP_CAST(__typeof__(cap), __ARCHCAP_CAST_PTRADDR(cap) & (mask))
#define archcap_address_or(cap, mask)                                        \
    __ARCHCAP_CAST(__typeof__(cap), __ARCHCAP_CAST_PTRADDR(cap) | (mask))
#define archcap_address_xor(cap, mask)                                       \
    __ARCHCAP_CAST(__typeof__(cap), __ARCHCAP_CAST_PTRADDR(cap) ^ (mask))
#define archcap_address_align_down(cap, alignment)                           \
    __extension__({                                                          \
        uintptr_t __cap_uptr0 = __ARCHCAP_CAST_UINTPTR(cap);                 \
        size_t __alignment_sz0 = __ARCHCAP_CAST_SIZE(alignment);             \
                                                                             \
        __cap_uptr0 &= ~(__alignment_sz0 - 1);                               \
                                                                             \
        (__ARCHCAP_CAST(__typeof__(cap), __cap_uptr0));                      \
    })

#define archcap_address_align_up(cap, alignment)                             \
    __extension__({                                                          \
        uintptr_t __cap_uptr0 = __ARCHCAP_CAST_UINTPTR(cap);                 \
        size_t __alignment_sz0 = __ARCHCAP_CAST_SIZE(alignment);             \
                                                                             \
        __cap_uptr0 += (__alignment_sz0 - 1);                                \
        __cap_uptr0 &= ~(__alignment_sz0 - 1);                               \
                                                                             \
        (__ARCHCAP_CAST(__typeof__(cap), __cap_uptr0));                      \
    })
#define archcap_address_diff(cap1, cap2)                                     \
    (archcap_address_get(cap1) - archcap_address_get(cap2))
#define archcap_address_clear_bits(cap, mask)                                \
    archcap_address_and(cap, ~(mask))
#define archcap_address_get_bits(cap, mask)                                  \
    (archcap_address_get(cap) & (mask))
#define archcap_is_same(cap1, cap2)                                          \
    (archcap_address_get(cap1) == archcap_address_get(cap2))
#define archcap_perms_get(cap)                                               \
    ((void) (cap), ARCHCAP_PERM_MASK_ALL)
#define archcap_perms_clear(cap, perms)                                      \
    ((void) (perms), cap)
#define archcap_perms_and(cap, perms)                                        \
    ((void) (perms), cap)
#define archcap_perms_set(cap, perms)                                        \
    ((void) (perms), cap)
#define archcap_perms_has(cap, perms)                                        \
    ((void) (cap), (void) (perms), true)
#define archcap_base_get(cap)                                                \
    __extension__({                                                          \
        ptraddr_t __null_addr = 0x0;                                         \
        __null_addr;                                                         \
    })
#define archcap_limit_get(cap)                                               \
    __extension__({                                                          \
        ptraddr_t __null_addr = 0x0;                                         \
        ~__null_addr;                                                        \
    })
#define archcap_length_get(cap)                                              \
    __extension__({                                                          \
        size_t __zero = 0x0;                                                 \
        ~__zero;                                                             \
    })
#define archcap_is_in_bounds(cap, size)                                      \
    ((void) (cap), (void) (size), true)
#define archcap_representable_length(length)                                 \
    (length)
#define archcap_representable_alignment_mask(length)                         \
    (__ARCHCAP_CAST_PTRADDR(~0UL))
#define archcap_bounds_set(cap, length)                                      \
    ((void) (length), cap)
#define archcap_bounds_set_exact(cap, length)                                \
    ((void) (length), cap)
#define archcap_bounds_set_base_length(cap, base, length)                    \
    ((void) (base), (void) (length), cap)
#define archcap_bounds_set_exact_base_length(cap, base, length)              \
    ((void) (base), (void) (length), cap)
#define archcap_bounds_set_base_limit(cap, base, limit)                      \
    ((void) (base), (void) (limit), cap)
#define archcap_bounds_set_exact_base_limit(cap, base, limit)                \
    ((void) (base), (void) (limit), cap)
#define archcap_cap_build(cap, key)                                          \
    ((void) (key), cap)

#endif /* !ARCHCAP_ABI_PURECAP */


/* Part of the implementation, which is common
 * for capability and non-capability pointers */

/* Permissions: Load */
#define archcap_perms_has_load(cap)                                          \
    archcap_perms_has(cap, ARCHCAP_PERM_LOAD)
#define archcap_perms_has_load_cap(cap)                                      \
    archcap_perms_has(cap, ARCHCAP_PERM_LOAD_CAP)

/* Permissions: Store */
#define archcap_perms_has_store(cap)                                         \
    archcap_perms_has(cap, ARCHCAP_PERM_STORE)
#define archcap_perms_has_store_cap(cap)                                     \
    archcap_perms_has(cap, ARCHCAP_PERM_STORE_CAP)
#define archcap_perms_has_store_local_cap(cap)                               \
    archcap_perms_has(cap, ARCHCAP_PERM_STORE_LOCAL_CAP)

/* Permissions: Execute */
#define archcap_perms_has_execute(cap)                                       \
    archcap_perms_has(cap, ARCHCAP_PERM_EXECUTE)

/* Permissions: Load capability with write permissions */
#define archcap_perms_has_mutable_load(cap)                                  \
    archcap_perms_has(cap, ARCHCAP_PERM_MORELLO_MUTABLE_LOAD)

/* Permissions: Can be stored as a global capability */
#define archcap_perms_has_global(cap)                                        \
    archcap_perms_has(cap, ARCHCAP_PERM_GLOBAL)

/* Permissions: Is a compartment ID */
#define archcap_perms_has_compartment_id(cap)                                \
    archcap_perms_has(cap, ARCHCAP_PERM_MORELLO_COMPARTMENT_ID)

/* Permissions: Non-restricted execution mode */
#define archcap_perms_has_executive(cap)                                     \
    archcap_perms_has(cap, ARCHCAP_PERM_MORELLO_EXECUTIVE)

/* Permissions: Access to system registers */
#define archcap_perms_has_system(cap)                                        \
    archcap_perms_has(cap, ARCHCAP_PERM_SYSTEM)

/* Permissions: Sealing */
#define archcap_perms_has_seal(cap)                                          \
    archcap_perms_has(cap, ARCHCAP_PERM_SEAL)
#define archcap_perms_has_unseal(cap)                                        \
    archcap_perms_has(cap, ARCHCAP_PERM_UNSEAL)
#define archcap_perms_has_branch_sealed_pair(cap)                            \
    archcap_perms_has(cap, ARCHCAP_PERM_MORELLO_BRANCH_SEALED_PAIR)


#if ARCHCAP_ABI_HYBRID
#define __ARCHCAP_HYBRID_CAPABILITY_ATTR __capability
#else /* !ARCHCAP_ABI_HYBRID */
#define __ARCHCAP_HYBRID_CAPABILITY_ATTR
#endif

/* Set bounds according to the type with base at the current value
 * Bounds can be rounded outwards if otherwise non-representable */
#define archcap_bounds_set_from_type(T, cap)                                 \
    __extension__({                                                          \
        __typeof__(cap) __cap_ptr3 = (cap);                                  \
                                                                             \
        __cap_ptr3 = archcap_bounds_set(__cap_ptr3, sizeof(T));              \
                                                                             \
        __ARCHCAP_CAST(T* __ARCHCAP_HYBRID_CAPABILITY_ATTR, __cap_ptr3);     \
    })

/* Set bounds according to the type with base at the current value.
 * Bounds can't be rounded even if non-representable - otherwise,
 * the capability tag is cleared */
#define archcap_bounds_set_exact_from_type(T, cap)                           \
    __extension__({                                                          \
        __typeof__(cap) __cap_ptr3 = (cap);                                  \
                                                                             \
        __cap_ptr3 = archcap_bounds_set_exact(__cap_ptr3, sizeof(T));        \
                                                                             \
        __ARCHCAP_CAST(T* __ARCHCAP_HYBRID_CAPABILITY_ATTR, __cap_ptr3);     \
    })

#if defined(__cplusplus)
__ARCHCAP_START_CPP_SECTION
template<typename T, typename U>
T* __ARCHCAP_HYBRID_CAPABILITY_ATTR archcap_cast(U cap) {
    return archcap_bounds_set_exact_from_type(T, cap);
}
__ARCHCAP_END_CPP_SECTION
#endif /* __cplusplus */

/* Construction of capabilities which aren't supposed to be dereferenced */
#define archcap_nonderef_cast_for_type(T, value)                             \
    __extension__({                                                          \
        __PTRADDR_TYPE__ __value_sz2 = __ARCHCAP_CAST_PTRADDR(value);        \
        uintptr_t __null_cap2 = 0x0;                                         \
                                                                             \
        (__ARCHCAP_CAST(T*, archcap_address_set(__null_cap2, __value_sz2))); \
    })

#if defined(__cplusplus)
__ARCHCAP_START_CPP_SECTION
template<typename T, typename U>
T* archcap_nonderef_cast(U value) {
    return archcap_nonderef_cast_for_type(T, value);
}
__ARCHCAP_END_CPP_SECTION
#endif /* __cplusplus */
