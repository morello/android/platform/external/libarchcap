/*
 * Copyright (c) 2020 Arm Limited. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#define __ARCHCAP_START_CPP_SECTION extern "C++" {
#define __ARCHCAP_END_CPP_SECTION }

#include "archcap-attributes.h"
#include "archcap-defs.h"

#define __ARCHCAP_INTERNAL_HEADERS_ALLOWED

#include "archcap-asserts.h"
#include "archcap-casts.h"

#if ARCHCAP_CAPABILITIES_AVAILABLE
#include "archcap-operations-caps.h"
#endif /* ARCHCAP_CAPABILITIES_AVAILABLE */

#include "archcap-operations-ptrs.h"

#undef __ARCHCAP_INTERNAL_HEADERS_ALLOWED

#undef __ARCHCAP_START_CPP_SECTION
#undef __ARCHCAP_END_CPP_SECTION
